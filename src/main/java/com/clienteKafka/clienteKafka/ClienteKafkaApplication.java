package com.clienteKafka.clienteKafka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClienteKafkaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClienteKafkaApplication.class, args);
	}

}
