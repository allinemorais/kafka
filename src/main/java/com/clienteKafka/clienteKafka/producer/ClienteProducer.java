package com.clienteKafka.clienteKafka.producer;

import com.clienteKafka.clienteKafka.model.Cliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class ClienteProducer {
    @Autowired
    private KafkaTemplate<String, Cliente> producer;

    public void gravarLog(Cliente cliente)
    {
        producer.send("spec2-alline-lopes-1",cliente);
    }
}

