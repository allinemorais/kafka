package com.clienteKafka.clienteKafka.controller;

import com.clienteKafka.clienteKafka.model.Cliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import com.clienteKafka.clienteKafka.producer.ClienteProducer;

@RestController
@RequestMapping("/acesso")
public class ClienteController {

    @Autowired
    private ClienteProducer clienteProducer;

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    public HttpStatus create(@RequestBody Cliente cliente)
    {
        clienteProducer.gravarLog(cliente);
        return   HttpStatus.CREATED;
    }


    @GetMapping("/pagamentos/{id}")
    public void create()
    {

    }

}
